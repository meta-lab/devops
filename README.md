# Meta-Lab

## espacio de desarrollo sistematizado para probar y publicar aplicaciones

técnicamente seria integrar, en un flujo de trabajo ágil, un conjunto de herramientas como ansible, vagrant, docker, git, ssh, webmin, ide...

### algunos proyectos en esta la misma linea:
["devops tools for everyone" presented at DevCon in Warsaw by michal.karzynski.pl](http://michal.karzynski.pl/blog/2014/06/22/devops-tools-for-everyone-vagrant-puppet-webmin/)
[oasis.sandstorm.io Cloud Platform](https://oasis.sandstorm.io/shared/ds776h6ePhz1IENCrpk_3l63lKe79LGpDLYq6IcWYDn)

pero poner tanto énfasis en la organización como en las tecnologías

### diseño organizacional
basando el diseño organizacional en el trabajo de kurt lewin:
["Organization development : science, technology, or philosophy?" by Schein, Edgar H.](http://dspace.mit.edu/handle/1721.1/2267)